#!/usr/bin/python3
import numpy as np
from numba import jit
from numba import njit,objmode

reel=np.float32

@jit(nopython=True)
def _Z(x,X,h,j):        
    expo = np.sum((x-X)**2/(2*h**2),axis = 1,dtype=reel)
    carre = (x[j]-X[:,j])**2 -h[j]**2        
    return(np.mean(carre*np.exp(-expo)))

@jit(nopython=True)
def _gaussian_kernel(vec):
    return(np.exp(-(np.linalg.norm(vec)**2)/2)/np.sqrt(2*np.pi))


@jit(nopython=True)
def _density_estimator(ker,x,X,h):    
    coeff = 1/np.prod(h)        
    expo = np.sum((x-X)**2/(2*h**2),axis = 1,dtype=reel)        
    return(coeff*np.mean(-expo))

def _bootstrap_variance_estimation(nb_steps,size_sample,x,X,h,j):
    #dim
    n = X.shape[0]
    Z_a_moy = []   
    for i in range(nb_steps):
        rng = np.random.default_rng()        
        sample = X[rng.choice(n, size=size_sample, replace=False),:]
        Z_a_moy.append(_Z(x,sample,h,j))
        
    return np.var(Z_a_moy,dtype=reel)


def _density_estim_rodeo(X,x,beta,c_0,c_n,nb_steps,size_sample):
    #dimensions
    n = X.shape[0]
    d = X.shape[1]
    #initalize
    #h = np.asarray([c_0 for i in range(d)],dtype = reel) #avant il y avait h_0: à creuser
    h = np.full((d),c_0,dtype = reel)
    A = [j for j in range(d)]
    #loop : tant que A n'est pas vide
    while(A) :
        for j in A : 
            #calcul de Z_j et s_2j
            Z_j = _Z(x,X,h,j)
            s_2j = _bootstrap_variance_estimation(nb_steps,size_sample,x,X,h,j)
            
            lambda_j = np.sqrt(s_2j*2*np.log(n*c_n))
            if(abs(Z_j) > lambda_j):
                h[j] = beta*h[j]
            else:
                A.remove(j)
                
    return(h)

class Rodeo:
    def __init__(self):
        self.estimations=[]

    def fit(self,X,Xtest):
        from tqdm import tqdm
        n = X.shape[0]
        dim = X.shape[1]
        density = []
        c_0  = 1
        h_0 = c_0/np.log(np.log(n))
        beta = 0.9
        c_n = np.log(dim)
        nb_steps = 50
        size_sample = 500

        for i in tqdm(range(X.shape[0])):
            #print("Individu : {}".format(i))
            x = Xtest[i,:]
            h = np.array(_density_estim_rodeo(X,x,beta,c_0,c_n,nb_steps,size_sample))            
            f_fonc = _density_estimator(_gaussian_kernel,x,X,h)
            density.append(f_fonc)
        return density