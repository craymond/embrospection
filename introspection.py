#!/usr/bin/python3
from re import I
import pandas as pd
import sys
import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.utils import shuffle
from sklearn.utils.extmath import softmax
from tqdm import tqdm

QUICKDEBUG=0
NBEMBEDDING=100

NBJOBS=4


def extract_global_info_from_local_densities(densities):
    nbinfo=3
    max = densities.max()
    min = densities.min()
    info=np.zeros(shape=(densities.shape[0],nbinfo),dtype=np.int)
    for i,l in enumerate(densities):
        info[i,0]=(l <(max-min)/3).sum()
        info[i,1]=(l <(max-min)/2).sum()
        info[i,2]=(l <(max-min)/1.5).sum()
    return info

def plot_variable_distribution(vec):
    print('\n','vec.shape=',vec.shape)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(figsize=(15,15))
    for i in range(len(vec[0])):    
        # Set up the plot
        ax = plt.subplot(2, 4, i+1)        
        # Draw the plot
        ax.hist(vec[:,i], bins = 180, color = 'blue', edgecolor = 'blue')        
        # Title and labels
        ax.set_title('Histogramme pour la cordonnées %d' % (i+1), size = 10)
        ax.set_xlabel('Coordonnées vectorielles', size = 5)
       
    fig, axes = plt.subplots(figsize=(20,10), ncols=2, nrows=2)
    import seaborn as sns
    sns.kdeplot(vec[:,0], ax=axes[0,0]).set_title('Coordonées 1')
    sns.kdeplot(vec[:,1], ax=axes[0,1]).set_title('Coordonées 2')
    plt.tight_layout() 
    plt.show()


def extract_global_independant_proba(train,test):
    """compute local density for each embedding variable on the train and estimate 
        global proba of the test embedding to have been emmited by these densities
    """
    from sklearn.model_selection import GridSearchCV
    from sklearn.neighbors import KernelDensity
    print(train.shape)
    print(test.shape)
    assert(len(train[0])==100 and len(test[0])==100) #embedding de taille 100

    density = np.zeros(shape=(test.shape[0],test.shape[1]),dtype='float')
    best_params=[]
    for i in tqdm(range(NBEMBEDDING)) : 
        tunedkd = KernelDensity(kernel='gaussian',atol=QUICKDEBUG,rtol=QUICKDEBUG,bandwidth=0.001)
        #tunedkd = GridSearchCV(KernelDensity(kernel='gaussian'), {'bandwidth': np.linspace(0, 0.2,10)}, cv=2,n_jobs=NBJOBS) # 5-fold cross-validation
        X = train[:,i].reshape(-1, 1)
        x = test[:,i].reshape(-1, 1)
        tunedkd.fit(X)
        density[:,i]=tunedkd.score_samples(x)       
        #best_params.append(tunedkd.best_params_)        
        #import pandas as pd
        #r=pd.DataFrame(tunedkd.cv_results_)
        #sys.stderr.write(r.to_string())
    #plot_variable_distribution(test[:,0:2])
    #plot_variable_distribution(density[:,0:2])
   
    #compute global proba
    #print(best_params)
    #exit()
    proba =np.sum(density,axis=1)  
    print(density[0:2,:])
    print(proba[0:2])
   
    return proba,density

def extract_global_dependant_proba(train,test):
    """compute global density for embedding on the train and estimate 
        global proba of the test embedding to have been emmited by this density
    """
    from sklearn.model_selection import GridSearchCV
    from sklearn.neighbors import KernelDensity
    assert(len(train[0])==100 and len(test[0])==100) #embedding de taille 100
    
    tunedkd = KernelDensity(kernel='gaussian',atol=QUICKDEBUG,rtol=QUICKDEBUG,bandwidth=0.02)
    #tunedkd = GridSearchCV(KernelDensity(kernel='gaussian'), {'bandwidth': np.linspace(0.001, 0.5, 7)}, cv=3,n_jobs=NBJOBS) # 5-fold cross-validation
    tunedkd.fit(train)
    #print(tunedkd.best_params_)      
    proba=tunedkd.score_samples(test) 
    return proba    
            


def extract_Y(data):
    """permet d'extraire deux predictions pour la classif: erreur ou non et les 4 clusters ACNCE
    """
    #indices 2 et 3 sont les hyp et ref   
    Yerror = ["ERR" if line[2] != line[3] else "OK" for line in data]
    Ycluster = [line[-1] for line in data]
    return Yerror, Ycluster

def read_csv_as_list_of_list(name,delim=' '):
    from csv import reader    
    with open(name, 'r') as read_obj:
        csv_reader = reader(read_obj,delimiter=delim)
        list_of_rows = list(csv_reader)
    #filter empty lines
    final = [e for e in list_of_rows if len(e)>2]
    return final

def dump_data(data,Y,output):
    res = []  
    for i in range(len(data[0])):        
        line = []
        for info in data:
            if hasattr(info[i], '__iter__'):  
                line = line + [e for e in info[i]]
            else:
                line.append(info[i])
        line.append(Y[i]+'.') #tag is a string (iterable) so we remove from the loop, we process it beside and add a "." to produce a .data compatible file
        res.append(line)
    import csv
    with open(output,'w') as f:
        write = csv.writer(f)        
        write.writerows(res)

def run_rodeo(train_emb,test_emb):
    from rodeo import Rodeo
    estimator = Rodeo()
    res = estimator.fit(train_emb,test_emb)
    return res

def get_word_nearest_neighbor_distance(train,test):
      
    from sklearn.neighbors import KernelDensity
    word_embs=dict()
    word_amb=dict()
    kernel_w=dict()
    wordlist=set([e[0] for e in train])
    print(wordlist)
    for e in wordlist:
        word_embs[e]=np.array([row[5:105] for row in train if row[0]==e],dtype='float')
        kernel_w[e]=KernelDensity(kernel='gaussian',atol=QUICKDEBUG,rtol=QUICKDEBUG,bandwidth=0.002)
        kernel_w[e].fit(word_embs[e])
        #nb annotattion for a word
        word_amb[e] = len(set([row[3] for row in train if row[0]==e]))
    

    from sklearn.metrics.pairwise import cosine_similarity,euclidean_distances,pairwise_distances
    res =[]
    notvu=0
    for row in test:
        feat=[]
        emb=np.array(row[5:105],dtype='float')        
        
        if row[0] not in word_embs: #mot pas vu
            notvu+=1
            for i in range(8):
                feat.append(0)           
        else:
            feat.append(len(word_embs[row[0]])) #nombre de ce mot vu dans le train
            cos=pairwise_distances(emb.reshape(1, -1),word_embs[row[0]],metric='cosine')
            feat.append(np.mean(cos))
            feat.append(np.max(cos))
            feat.append(np.min(cos))
            feat.append(kernel_w[row[0]].score_samples(emb.reshape(1, -1)))  
            feat.append(word_amb[row[0]])
            feat.append(np.count_nonzero(cos>0.5,axis=1)/len(cos))
            feat.append(np.count_nonzero(cos<0.1,axis=1)/len(cos))
        res.append(feat)
    print('notvu=',notvu)
    return np.array(res,dtype='float')



def get_nearest_neighbor_distance(train,test):
    from sklearn.metrics.pairwise import cosine_similarity,euclidean_distances,pairwise_distances
    from numpy import linalg as LA
    print('Compute cosine sim...')
    rescos=pairwise_distances(test,train,metric='cosine',n_jobs=NBJOBS)#hpe the same as cosine_sim but with njobs
    #print('Compute euclidean dist...')
    #reseuc=pairwise_distances(test,train,metric='euclidean',n_jobs=NBJOBS)
    minormontrain=LA.norm(train,axis=1).min()
   
    print('minnormontrain=',minormontrain)
    #print('maxnormontrain=',LA.norm(train,axis=1).max())
    normtest=LA.norm(test,axis=1).reshape(1,len(test))
    moynormtes=LA.norm(train,axis=1).mean()

    print('norm>min',np.unique(normtest > minormontrain,return_counts=True))
    print('norm>moy',np.unique(normtest > moynormtes,return_counts=True))
   
    res= np.zeros(shape=(len(test),8),dtype='float')
    res[:,0]=np.max(rescos,axis=1)
    res[:,1]=np.mean(rescos,axis=1)
    res[:,2]=np.std(rescos,axis=1)#ecart type
    res[:,3]=np.count_nonzero(rescos>0.5,axis=1)/len(train) #percentage of the train set whose emb has cos +0.5
    res[:,4]=np.count_nonzero(rescos>0.8,axis=1)/len(train) #percentage of the train set whose emb has cos +0.5
    res[:,5]=np.count_nonzero(rescos<0.1,axis=1)/len(train) #percentage of the train set whose emb has cos +0.5
    res[:,6]=normtest > minormontrain
    res[:,7]=normtest > moynormtes
    #res[:,4]=np.max(reseuc,axis=1)
    #res[:,5]=np.sum(reseuc,axis=1)/len(train)
    #res[:,6]=np.count_nonzero(reseuc<0.5,axis=1)/len(train)
    #res[:,7]=np.count_nonzero(reseuc<0.8,axis=1)/len(train)
    return res


def generate_costly_data(trainf,testf,output_stem):
    train = read_csv_as_list_of_list(trainf)
    test = read_csv_as_list_of_list(testf)

    print('train.shape=(',len(train),',',len(train[0]),')')
    print('test.shape=(',len(test),',',len(test[0]),')')
    print('output_stem=',output_stem)
    train_emb=np.array([line[5:105] for line in train],dtype='float')
    test_emb=np.array([line[5:105] for line in test],dtype='float')

    softmax_proba=[float(line[4]) for line in test]
    predict_an_O=[float(line[3]=='O') for line in test]

    wordbased_distance_features=get_word_nearest_neighbor_distance(train,test)
    #print(wordbased_distance_features[0:2])
    min_sum_distance_to_train=get_nearest_neighbor_distance(train_emb,test_emb)
    
    #res=run_rodeo(train_emb,test_emb)
    #print(res)
    #exit()
    
    #too long and looks useless
    #probaIndependant,densities=extract_global_independant_proba(train_emb,test_emb)
    probaDependant=extract_global_dependant_proba(train_emb,test_emb)

    #print(info)
    #dump bonzai data
    Ye,Yc= extract_Y(test)
  
    dump_data([softmax_proba,predict_an_O,min_sum_distance_to_train,probaDependant,wordbased_distance_features],Ye,output_stem+"_err.data")
    dump_data([softmax_proba,predict_an_O,min_sum_distance_to_train,probaDependant,wordbased_distance_features],Yc,output_stem+"_clu.data")

def load_ready_data_for_classification(file):
    data = read_csv_as_list_of_list(file+".data",delim=',')
    Ystring=[e[-1] for e in data]
    tmplabels=set(Ystring)
    labels={}   
    i=0
    for key in tmplabels:
        labels[key]=i
        i+=1
    for line in data:
        line[-1]=labels[line[-1]]
    names= open(file+'.names').readlines()
    attribute = [names[i].split(':')[0] for i in range(1,len(names))]
    print(attribute)
    return np.array(data,dtype='float')[:,0:-1],[int(e[-1]) for e in data],Ystring,attribute, list(labels)

def test():
    import numpy as np
    from sklearn import datasets
    import matplotlib.pyplot as plt
    from yellowbrick.target.feature_correlation import feature_correlation

    #Load the diabetes dataset
    data = datasets.load_iris()
    X, y = data['data'], data['target']
    print(X[0])
    print(y[0:3])
    features = np.array(data['feature_names'])
    visualizer = feature_correlation(X, y, labels=features)
    plt.tight_layout()        # Finalize and render the figure

def correlation_between_variables(X,Y,labels):
    from sklearn.feature_selection import SelectKBest
    from sklearn.feature_selection import f_classif ,mutual_info_classif
    from matplotlib import pyplot
    # configure to select all features
    fs = SelectKBest(score_func=mutual_info_classif, k='all')
    # learn relationship from training data
    fs.fit(X, Y)
    # transform train input data
    #X = fs.transform(X)
    #for i in range(len(fs.scores_)):
    #    print('Feature %d: %f' % (i, fs.scores_[i]))
    # plot the scores
    pyplot.barh(labels, fs.scores_,)
    pyplot.show()


def classify(X,Y,labels,linear=True):
    from sklearn.preprocessing import MinMaxScaler
    X = MinMaxScaler().fit_transform(X)
    from sklearn.linear_model import LogisticRegressionCV,RidgeClassifierCV
    from sklearn.model_selection import cross_val_predict,GridSearchCV,StratifiedKFold
    from sklearn.metrics import classification_report    
    from sklearn.ensemble import GradientBoostingClassifier
    from sklearn.neural_network import MLPClassifier
    from mlp import MLPclassifierKeras
    #clf = LogisticRegressionCV(cv=10, random_state=0)
    clf = RidgeClassifierCV(cv=10)
    if not linear:
        #clf = GridSearchCV(GradientBoostingClassifier(),param_grid={'n_estimators':[5,7,10,20,50,75,100]},cv=5,scoring='f1_micro',n_jobs=2)
        #clf = GradientBoostingClassifier(n_estimators=100,max_depth=2)       
        clf = GradientBoostingClassifier(n_iter_no_change=5,validation_fraction=0.1)
        #clf = MLPclassifierKeras(10)
        #print(pd.DataFrame(clf.cv_results_))
    
   
    #only softmax
    NBCV=236
    from sklearn.model_selection import KFold
    predsoft=cross_val_predict(clf, X[:,0].reshape(-1, 1), Y, cv=NBCV,n_jobs=NBJOBS)
    predcomp=cross_val_predict(clf, X[:,1:], Y, cv=NBCV,n_jobs=NBJOBS)
    #predcomp=cross_val_predict(clf2, X[:,0].reshape(-1, 1), Y, cv=NBCV,n_jobs=NBJOBS)
    predall=cross_val_predict(clf, X, Y, cv=NBCV,n_jobs=NBJOBS)

    print('softmax only\n',classification_report(Y,predsoft,target_names=labels,digits=4))
    print('sauf softmax\n',classification_report(Y,predcomp,target_names=labels,digits=4))
    print('everything\n',classification_report(Y,predall,target_names=labels,digits=4))

    softmaxres = [1 if Y[i]==predsoft[i] else 0 for i in range(len(predsoft))]
    compres = [1 if Y[i]==predcomp[i] else 0 for i in range(len(predsoft))]
    allres = [1 if Y[i]==predall[i] else 0 for i in range(len(predsoft))]
    import scipy
    print('comp vs softmax:',scipy.stats.ttest_rel(softmaxres,compres))
    print('all vs softmax:',scipy.stats.ttest_rel(softmaxres,allres))



if __name__ == '__main__':
    
    import time
    start = time.perf_counter()
    if len(sys.argv)>2:
        generate_costly_data(sys.argv[1],sys.argv[2],sys.argv[3])
        end = time.perf_counter()   
        from humanfriendly import format_timespan
        print('Processing finished in ',format_timespan((end - start)))
        exit()
    else:
        X,Y,Ystring,names,labels=load_ready_data_for_classification(sys.argv[1])

    # #extract infos from local densities
    #info=extract_global_info_from_local_densities(densities)

    #correlation between feature and class
    from yellowbrick.target.feature_correlation import feature_correlation
    #visualizer = feature_correlation(X, Y, labels=names)
    
    #from scipy.stats import kendalltau
    #coef, p = kendalltau(X, y)
    
    #correlation between variables:
    #correlation_between_variables(X,Y,names)

    
    #classification
    #classify(X,Y,labels)
    classify(X,Y,labels,False)
    
    #clf.fit(X, Y)
    #print(clf.coef_)
    end = time.perf_counter()   
    from humanfriendly import format_timespan
    print('Processing finished in ',format_timespan((end - start)))

    
    
