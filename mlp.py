from tabnanny import verbose
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
from sklearn.base import BaseEstimator, ClassifierMixin
import numpy as np
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

class MLPclassifierKeras(BaseEstimator):

    def __init__(self,n_iter=10):
        self.n_iter=10
        self.model=""		
        
        
    def __build(self,input,hidden,output):       
        entree = tf.keras.Input(shape=(input,))
        #tmp = BatchNormalization()(
        tmp = tf.keras.layers.Dense(hidden, activation='sigmoid')(entree)
        tmp = tf.keras.layers.Dropout(0.5)(tmp)
        out = tf.keras.layers.Dense(output, activation='softmax')(tmp)
        self.model = tf.keras.Model(outputs=out,inputs=entree)
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=["accuracy"])		
        #self.model.summary()		

    def fit(self,X,Y,batch=4):
        self.__build(X.shape[1],20,len(set(Y)))
        self.model.fit(X, np.asarray(Y,dtype='int'), epochs=self.n_iter, batch_size=batch,verbose=0)
        
    def predict(self,X):
        classes = self.model.predict(X, batch_size=X.size,verbose=False).argmax(-1)
        return classes